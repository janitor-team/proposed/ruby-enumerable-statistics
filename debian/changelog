ruby-enumerable-statistics (2.0.7+dfsg-3) unstable; urgency=medium

  * Team upload
  * debian/changelog: add bug number retroactively
  * debian/ruby-tests.rake: skip tests on i386 as they need SSE2.
    See https://github.com/mrkn/enumerable-statistics/issues/31 for the
    discussion with upstream regarding this.
  * debian/rules: don't install documentation sources

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 23 Jan 2022 13:36:13 -0300

ruby-enumerable-statistics (2.0.7+dfsg-2) unstable; urgency=medium

  * Team upload
  * Add patch to avoid testing floats for equality.
    Fixes the build on i386. (Closes: #996621)

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 03 Nov 2021 08:45:26 -0300

ruby-enumerable-statistics (2.0.7+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ Antonio Terceiro ]
  * debian/watch: download from github to get tests etc
  * New upstream version 2.0.7+dfsg
    - Builds fine against ruby3.0 (Closes: #996219)
  * debian/ruby-tests.rake: run tests with rspec
  * debian/rules: remove bogus bin/rspec that gets installed
  * Bump Standards-Version to 4.6.0; no changes needed

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 15 Oct 2021 08:14:17 -0300

ruby-enumerable-statistics (2.0.1+dfsg-3) unstable; urgency=medium

  * Remove git usage in gemspec (Closes: #956405)

 -- Pirate Praveen <praveen@debian.org>  Sat, 18 Apr 2020 10:33:27 +0000

ruby-enumerable-statistics (2.0.1+dfsg-2) unstable; urgency=medium

  * Source only upload for testing migration

 -- Pirate Praveen <praveen@debian.org>  Thu, 09 Apr 2020 15:12:01 +0530

ruby-enumerable-statistics (2.0.1+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #955197)

 -- Pirate Praveen <praveen@debian.org>  Sat, 28 Mar 2020 14:19:57 +0530
